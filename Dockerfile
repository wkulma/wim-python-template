FROM python

ADD . /template

WORKDIR /template

RUN pip install --upgrade --quiet pip && \
    pip install -r requirements.txt

ENTRYPOINT python app/engine/consumer.py
