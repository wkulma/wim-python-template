import os

from app.service.resources import Resources

MyService = Resources


mq_config = {
    'host': os.environ["AMQ_ADDRESS"],
    'port': os.environ["AMQ_PORT"],
    'user': os.environ["AMQ_USER"],
    'password': os.environ["AMQ_PASSWORD"],
    'destination': os.environ["AMQ_DESTINATION"]
}